require('express-async-errors');
const express = require('express');
const router = require('./src/routes');
const { error } = require('./src/middlewares');
const { socketController } = require('./src/controllers');
const { broker } = require('./src/libs');

async function initApp() {
    const app = express();
    const channel = await broker.initChatrooms(3);
    app.socket = socketController.initRooms(channel);
    app.use(express.json());
    app.use('/', router);
    app.use(error);
    app.use("/public", express.static(__dirname + '/public'));
    app.use("/public/login", express.static(__dirname + '/public'));
    app.use("/public/chat", express.static(__dirname + '/public'));
    return app;
}

module.exports = initApp;