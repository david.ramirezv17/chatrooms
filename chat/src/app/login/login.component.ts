import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SocketService } from '../services/socket.service';
import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  private colorlogin: string = 'primary';
  private colorreg: string = 'primary';
  private inputUser: string = '';
  private inputPassword: string = '';
  private responseLabel: string = '';
  private rooms: any = [];
  private selectedRoom: string = '';

  constructor(private http: HttpClient, private router: Router, private socketService: SocketService) { }
  async login() {
    try {
      if (this.selectedRoom) {
        let pwd = CryptoJS.AES.encrypt(this.inputPassword, '%$r&1$9b7F9$80$').toString();
        let requestBody = {
          userId: this.inputUser,
          pwd
        }
        this.socketService.setRoom(this.selectedRoom);
        const response: any = await this.http.post('http://localhost:3312/login', requestBody, { responseType: 'json' }).toPromise();
        sessionStorage.setItem('sessionData', this.inputUser);
        this.responseLabel = response.message;
        this.router.navigate(['chat']);
      } else {
        this.responseLabel = 'Please select a chatroom';
      }
    }
    catch (error) {
      this.responseLabel = error.error.message;
    }
  }
  async register() {
    try {
      let pwd = CryptoJS.AES.encrypt(this.inputPassword, '%$r&1$9b7F9$80$').toString();
      let requestBody = {
        userId: this.inputUser,
        pwd
      }
      const response: any = await this.http.post('http://localhost:3312/register', requestBody, { responseType: 'json' }).toPromise();
      this.responseLabel = response.message;
      this.inputPassword = '';
    }
    catch (error) {
      console.log(error);
      this.responseLabel = error.error.message;
    }
  }
  async ngOnInit(): Promise<any> {
    this.rooms = await this.http.get('http://localhost:3312/chat/rooms', { responseType: 'json' }).toPromise();
  }

}
