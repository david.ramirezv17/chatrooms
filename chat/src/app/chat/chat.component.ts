import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { SocketService } from '../services/socket.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.css']
})
export class ChatComponent implements OnInit {
  private inputText: any = '';
  private userId: string = '';
  private chatArea: string = '';
  private selectedRoom: string = '';

  constructor(private socketService: SocketService, private http: HttpClient) {
    this.userId = sessionStorage.getItem('sessionData');
    this.selectedRoom = this.socketService.getRoom();
  }
  async sendMessage() {
    let message = { userId: this.userId, message: this.inputText, date: Date.now(), chatroomId: this.selectedRoom };
    try {
      const response: any = await this.http.post('http://localhost:3312/chat', message).toPromise();
      this.inputText = '';
    } catch (error) {
      console.error(error);
    }
  }
  ngOnInit(): void {
    this.socketService.connect();
    this.socketService.getMessages().subscribe((message: Array<object>) => {
      let chatHistory = '';
      message.forEach(element => {
        chatHistory += '\n' + element['data'];
      });
      this.chatArea = chatHistory;
    });
  }

}
