import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { io } from 'socket.io-client';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private socket: any;
  private selectedRoom: string;
  constructor() { }

  connect() {
    this.socket = io('http://localhost:3312', {
      path: '/messages',
      query: {
        chatroomId: this.selectedRoom
      }
    });
  }

  getMessages() {
    return new Observable((observer) => {
      this.socket.on('message', (messages) => {
        if (messages && messages.length > 0) {
          observer.next(messages);
        }
      });
    });
  }

  sendMessage(message: object) {
    this.socket.emit('messages', JSON.stringify(message));
  }
  setRoom(room: string) {
    this.selectedRoom = room;
  }
  getRoom() {
    return this.selectedRoom;
  }
}
