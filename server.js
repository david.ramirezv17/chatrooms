
const PORT = '3312';
const LOCAL = true;
/** Función de lanzamiento del servidor
 * @returns {undefined}
*/
function launchServer() {
	if (LOCAL) {
		console.log(`🚀 Server starting on http://localhost:${PORT}`);
	}
}

(async () => {
	try {
		const app = await require('./app')();
		const server = app.listen(PORT, launchServer);
		app.socket.attach(server);
	} catch (error) {
		console.log('Error levantando aplicacion', error);
	}
})();