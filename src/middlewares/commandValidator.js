const { botController } = require('../controllers');

module.exports = (req, res, next) => {
	if (req.body.message.match(/(\/stock=).*/)) {
        botController.processCommand(req.body);
	}
    next();
};
