const amqp = require('amqplib');
const uniqid = require('uniqid');

const chatroomsController = require('../controllers/chatrooms.controller');
async function push(chatroomId, data) {
    const connection = await amqp.connect('amqp://localhost');
    const channel = await connection.createChannel();
    await channel.assertQueue(chatroomId);
    return await channel.sendToQueue(chatroomId, Buffer.from(JSON.stringify(data)));
}
async function initChatrooms(chatroomsNumber) {
    const connection = await amqp.connect('amqp://localhost');
    const channel = await connection.createChannel();
    for (let i = 0; i < chatroomsNumber; i++) {
        let chatroomId = `chatroom${i}`;
        chatroomsController.newChatroom(chatroomId);
        await channel.assertQueue(chatroomId);
        date = new Date();
        let msg = { date, data: `${date.toLocaleString()} || BOT:\nWelcome to chatroom${i}`, id: uniqid() };
        await channel.sendToQueue(chatroomId, Buffer.from(JSON.stringify(msg)));
    }
    return channel;
}
module.exports = {
    push,
    initChatrooms,
};
