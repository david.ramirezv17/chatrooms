global.usersRepository = [];
function getUser(userId) {
    let user = global.usersRepository.find((user) => user.id === userId);
    return user;
}
function saveUser(user) {
    global.usersRepository.push(user)
}
module.exports = {
    saveUser,
    getUser,
}