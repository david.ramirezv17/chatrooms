
global.chatroomsRepository = [];
function getChatrooms() {
    return global.chatroomsRepository;
}
function saveChatroom(id, history) {
    try {
        let chatroom = global.chatroomsRepository.find((chatroom) => chatroom.id === id);
        if (chatroom) {
            chatroom.history = history;
        } else {
            global.chatroomsRepository.push({ id, history });
        }
    } catch (error) {
        console.error(error);
        throw error;
    }
}
function getChatroomHistory(id) {
    let chatroom = global.chatroomsRepository.find((chatroom) => chatroom.id === id);
    chatroom.history.sort(function (message1, message2) { return message1.date - message2.date });
    chatroom.history = chatroom.history.slice(-50);
    return chatroom.history;
}
module.exports = {
    saveChatroom,
    getChatrooms,
    getChatroomHistory,
}