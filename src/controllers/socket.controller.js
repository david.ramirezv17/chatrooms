const { getHistory, getChatrooms, newMessage, checkMessage } = require('./chatrooms.controller');
const io = require('socket.io')();

function initRooms(channel) {
	io.path('/messages');
	io.on('connection', retrieveChatroomHistory);
	let chatrooms = getChatrooms();
	for (let chatroom of chatrooms) {
		channel.consume(chatroom.id, chatroomBroadcast.bind(null, channel));
	}
	return io;
}
function chatroomBroadcast(channel, message) {
	try {
		let chatroomId = message.fields.routingKey;
		let incommingMessage = JSON.parse(message.content.toString());
		if (!checkMessage(chatroomId, incommingMessage.id)) {
			let chatroomHistory = newMessage(chatroomId, incommingMessage);
			io.to(chatroomId).emit('message', chatroomHistory);
		}
		channel.ack(message);
	}
	catch (error) {
		console.error(error);
		throw error;
	}
}
function retrieveChatroomHistory(socket) {
	let chatroomId = socket.handshake.query.chatroomId;
	if(getChatrooms().some(room=>room.id===chatroomId))
	{
		socket.join(chatroomId);
		socket.emit('message', getHistory(chatroomId));
	};
}
module.exports = {
	initRooms
};
