const uniqid = require('uniqid');
const { broker } = require('../libs')
async function postMessage(req, res) {
	try {
		let date = new Date(req.body.date);
		let msg = { date, data: `${date.toLocaleString()} || ${req.body.userId}:\n${req.body.message}`, id: uniqid() };
		let result = await broker.push(req.body.chatroomId, msg);
		if (result) {
			res.status(200).json({ message: 'Message received' });
		} else {
			res.status(502).json({ message: 'Message not received' });;
		}
	} catch (error) {
		res.status(500).json({ message: 'Unknown Error' });;
		console.error(error);
	}
}
module.exports = {
	postMessage
};
