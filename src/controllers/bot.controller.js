const axios = require('axios');
const uniqid = require('uniqid');
const csv = require('csvtojson');
const { broker } = require('../libs');

async function processCommand(incommingMessage) {
	try {
		let stock = incommingMessage.message.split('=')[1];
		let url = `https://stooq.com/q/l/?s=${stock}&f=sd2t2ohlcv&h&e=csv`;
		let { data } = await axios.get(url);
		let jsonResponse = await csv().fromString(data);
		let date = new Date();
		let response = `${jsonResponse[0].Symbol} quote is $${jsonResponse[0].Open} per share`;
		let msg = { date, data: `${date.toLocaleString()} || BOT:\n${response}`, id: uniqid() };
		broker.push(incommingMessage.chatroomId, msg);
	} catch (error) {
		let date = new Date();
		let response = `Sorry, I can't help you right know, please try again`;
		let msg = { date, data: `${date.toLocaleString()} || BOT:\n${response}`, id: uniqid() };
		broker.push(incommingMessage.chatroomId, msg);
		console.log(error);
	}
}

module.exports = {
	processCommand,
};
