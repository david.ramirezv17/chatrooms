const { chatroomsModel } = require('../models');
function newChatroom(id) {
	try {
		chatroomsModel.saveChatroom(id, []);
	} catch (error) {
		console.log(error);
		throw error;
	}
}
function getHistory(id) {
	return chatroomsModel.getChatroomHistory(id);
}
function getChatrooms() {
	return chatroomsModel.getChatrooms();
}
function newMessage(chatroomId, incommingMessage) {
	let history = chatroomsModel.getChatroomHistory(chatroomId);
	history.push(incommingMessage);
	chatroomsModel.saveChatroom(chatroomId, history);
	return history;
}
function checkMessage(chatroomId, messageId) {
	let history = chatroomsModel.getChatroomHistory(chatroomId);
	return history.some(message => message.id === messageId);
}
function getRooms(req,res) {
	let rooms=chatroomsModel.getChatrooms().map(item => item.id);
	res.status(200).json(rooms);

}
module.exports = {
	newChatroom,
	getHistory,
	getChatrooms,
	newMessage,
	checkMessage,
	getRooms,
};
