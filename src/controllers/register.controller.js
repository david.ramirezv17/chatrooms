const { usersModel } = require('../models');
async function register(req, res) {
	let user = usersModel.getUser(req.body.userId);
	if (!user) {
		let user = {
			id: req.body.userId,
			pwd: req.body.pwd
		}
		usersModel.saveUser(user);
		res.status(200).json({ message: 'User created' });
	}
	else {
		res.status(400).json({ message: 'User already exists' });
	}
}

module.exports = {
	register,
};
