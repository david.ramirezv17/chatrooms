const crypto = require('crypto-js');
const { usersModel } = require('../models');
async function login(req, res) {
	try {
		let user = usersModel.getUser(req.body.userId);
		if (user) {
			let pwd = crypto.AES.decrypt(req.body.pwd, '%$r&1$9b7F9$80$').toString(crypto.enc.Utf8);
			let userPwd = crypto.AES.decrypt(user.pwd, '%$r&1$9b7F9$80$').toString(crypto.enc.Utf8);
			if (pwd === userPwd) {
				res.status(200).json({ message: 'Successful login' });
			}
			else {
				res.status(401).json({ message: 'Invalid password' });
			}
		}
		else {
			res.status(403).json({ message: 'Invalid user' });
		}
	} catch (error) {
		console.log(error);
	}
}
module.exports = {
	login,
};
