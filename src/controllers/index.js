const loginController = require('./login.controller');
const registerController = require('./register.controller');
const chatroomsController = require('./chatrooms.controller');
const chatController = require('./chat.controller');
const socketController = require('./socket.controller');
const botController = require('./bot.controller');
module.exports = {
    loginController,
    registerController,
    chatController,
    botController,
    socketController,
    chatroomsController,
};