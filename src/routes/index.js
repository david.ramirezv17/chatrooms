const express = require('express');
const login = require('./login');
const register = require('./register');
const chat = require('./chat');
const router = express.Router();

router.use('/login', login);
router.use('/register', register);
router.use('/chat', chat);

module.exports = router;
