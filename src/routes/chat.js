const express = require('express');
const { commandValidator } = require('../middlewares');
const { chatController,chatroomsController } = require('../controllers');
const router = express.Router();

router.route('/').post(commandValidator,chatController.postMessage);
router.route('/rooms').get(chatroomsController.getRooms);

module.exports = router;
