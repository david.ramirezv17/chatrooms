# Node Challenge
**Author:** David Enrique Ramírez Velásquez

To run this project you will need:
- NodeJS v12.18.2
- Docker
- Angular CLI v11.2.3
- Google Chrome

Initialization steps:
1. npm install
2. cd chat
3. npm install
4. npm run build
5. docker pull rabbitmq:3.8.14-management
6. docker run -d --hostname chat -p 5672:5672 -p 8081:15672 --name chat-rabbit rabbitmq:3.8.14-management
7. cd ..
8. npm run chat

Chat is now running!!
- Go to http://localhost:3312/public/login in Chrome Browser
- Register a user filling the fields
- Then select a chatroom and login
- Start chatting
- You can also open a new tab in chrome, register a second user and start chatting in the same chatroom.
